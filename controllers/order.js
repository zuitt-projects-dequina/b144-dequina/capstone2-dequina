const Order = require('../models/Order')
const Product = require('../models/Product');
const User = require('../models/User');
const auth = require('../auth');

// Getting All Orders
module.exports.getAllOrders = (userData) => {

	return User.findById(userData.userId).then(result => {
		if (userData.isAdmin != true) {
			return "Access denied. You are not an Admin."
		}
		else {
			return Order.find(userData.orders).then((result, error) => {
				if (error) {
					return false;
				}
				else {
					return result;
				}
			})
		}
	})
};


// // Retrieving authenticated user’s orders
// module.exports.myOrder = async (data, userData) => {

// 	return User.findById(userData.userId).then(result => {
// 		if (userData.isAdmin == true) {
// 			return "Access denied. You are an Admin."
// 		}
// 		else {

// 			return result.orders;
// 		}
// 	})
// }


// Retrieving authenticated user’s orders
module.exports.myOrder = async (data, userData) => {

	return User.findById(userData.userId).then(result => {
		if (userData.isAdmin == true) {
			return "Access denied. You are an Admin."
		}
		else {

			return result.orders;
		}
	})
}
